﻿using UnityEngine;
using System.Collections;

public class Souns : MonoBehaviour {

    public static Souns Instance;
    public AudioSource soundEffect;
    static AudioClip clickSound;


    private float lowPitchRange = 0.05f;
    private float highPitchRange = 1.05f;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    public void PlaySound(AudioClip clickSound)
    {
        soundEffect.Play();
    }

    
}
