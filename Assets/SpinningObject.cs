﻿using UnityEngine;
using System.Collections;

public class SpinningObject : MonoBehaviour {
    public float timer = 10f;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        transform.Rotate(0, 0, Time.deltaTime * 500);
        Physics.gravity = new Vector3(0, -500f, 0);
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            Destroy(gameObject); 
        }
    }
}
