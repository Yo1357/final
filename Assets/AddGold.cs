﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Timers;
using System.Collections.Generic;

public class AddGold : MonoBehaviour
{
        public GameObject minion;
        public static Timer aTimer;
        public static float gold;
        public static float goldPerSecond = 0;
        public static float goldPerClick = 1;
        private float doransRingCost = 100;
        private float doransBladeCost = 100;
        public float costIncrease = 1.20f;
        private float recurveBowCost = 1250;
        private float needlesslyLargeRodCost = 1600;
        private float rageBladeCost = 3000;
        private float lichBaneCost = 25500;
        private float mortalReminderCost = 10590;
        private float infinityEdgeCost = 450000000;
        private float rabadonsDeathCapCost = 450000000;
        private float triForceCost = 1900000000;

        public Text goldLevel;
        public Text addedGoldASecond;
        public Text goldPerClickText;
        public Text doransBladeText;
        public Text doransRingText;
        public Text recurveBowText;
        public Text needlesslyLargeRodText;
        public Text rageBladeText;
        public Text lichBaneText;
        public Text mortalReminderText;
        public Text infinityEdgeText;
        public Text rabadonsDeathCapText;
        public Text triForceText;

        public AudioClip clickSound;

    public GameObject lv1;
        public GameObject lv2;
        public int levelUp = 0;

        void Start()
        {
        goldLevel = GameObject.Find("GoldLevel").GetComponent<Text>();
        goldLevel.text = "Gold: " + gold;
        addedGoldASecond = GameObject.Find("goldPerSeconds").GetComponent<Text>();
        goldPerClickText = GameObject.Find("goldPerClick").GetComponent<Text>();
        doransBladeText = GameObject.Find("DoransBladeText").GetComponent<Text>();
        doransRingText = GameObject.Find("DoransRingText").GetComponent<Text>();
        recurveBowText = GameObject.Find("RecurveBowText").GetComponent<Text>();
        needlesslyLargeRodText = GameObject.Find("NeedlesslyLargeRodText").GetComponent<Text>();
        rageBladeText = GameObject.Find("RageBladeText").GetComponent<Text>();
        mortalReminderText = GameObject.Find("MortalReminderText").GetComponent<Text>();
        lichBaneText = GameObject.Find("LichBaneText").GetComponent<Text>();
        infinityEdgeText = GameObject.Find("InfinityEdgeText").GetComponent<Text>();
        rabadonsDeathCapText = GameObject.Find("RabadonsDeathCapText").GetComponent<Text>();
        triForceText = GameObject.Find("TriForceText").GetComponent<Text>();
        aTimer = new Timer(10000);
        aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
        aTimer.Interval = 100;
        aTimer.Enabled = true;
        gold = PlayerPrefs.GetFloat("gold");
        goldPerClick = PlayerPrefs.GetFloat("goldPerClick");
        goldPerSecond = PlayerPrefs.GetFloat("goldPerSecond");
        doransBladeCost = PlayerPrefs.GetFloat("doransBladeCost");
        doransRingCost = PlayerPrefs.GetFloat("doransRingCost");
        needlesslyLargeRodCost = PlayerPrefs.GetFloat("needlesslyLageRodCost");
        lichBaneCost = PlayerPrefs.GetFloat("lichBaneCost");
        rageBladeCost = PlayerPrefs.GetFloat("rageBladeCost");
        mortalReminderCost = PlayerPrefs.GetFloat("mortalReminderCost");
        infinityEdgeCost = PlayerPrefs.GetFloat("infinityEdgeCost");
        rabadonsDeathCapCost = PlayerPrefs.GetFloat("RabadonsDeathCapCost");
        triForceCost = PlayerPrefs.GetFloat("TriForceCost");
        if (goldPerClick < 1)
        {
            goldPerClick = 1;
            goldPerSecond = 0;
            doransRingCost = 100;
            doransBladeCost = 100;
            recurveBowCost = 1250;
            needlesslyLargeRodCost = 1600;
            rageBladeCost = 3000;
            mortalReminderCost = 10590;
            lichBaneCost = 25500;
            infinityEdgeCost = 450000000;
            rabadonsDeathCapCost = 450000000;
            triForceCost = 1900000000;
        }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            goldLevel.text = "Gold: " + gold;
            addedGoldASecond.text = "gold per second: " + goldPerSecond * 10;
            goldPerClickText.text = "gold per click: " + goldPerClick;
            doransRingText.text = "Dorans Ring : + 10 gold per seconds, Cost: " + doransRingCost;
            doransBladeText.text = "Dorans Blade : + 1 per click, Cost: " + doransBladeCost;
            recurveBowText.text = "Recurve Bow : + 7 per click, Cost: " + recurveBowCost;
            needlesslyLargeRodText.text = "Needlessly Large Rod : + 80 gold per second, Cost: " + needlesslyLargeRodCost;
            rageBladeText.text = "Rage Blade : + 100 gold per second and + 10 per click Cost: " + rageBladeCost;
            mortalReminderText.text = "Mortal Reminder : + 30 gold per click Cost: " + mortalReminderCost;
            lichBaneText.text = "Lich Bane: + 50 gold per second, increase gold per click by 15% Cost: " + lichBaneCost;
            infinityEdgeText.text = "Infinity Edge: + 10000 gold per click Cost:" + infinityEdgeCost;
            rabadonsDeathCapText.text = "Rabadons Death Cap: + 10000 gold per second Cost: " + rabadonsDeathCapCost;
        triForceText.text = "TriForce : + 10000 gold per second and + 10000 gold per click and increases gold per second and gold per click by 10% Cost: " + triForceCost;
            if (levelUp < 1 && gold >= 1000000){
            lv1.SetActive(false);
            lv2.SetActive(true);
            levelUp++;
            
            }
    }
        public void Click()
        {
            gold += goldPerClick;
            goldLevel.text = "Gold: " + gold + "\n       + " + goldPerClick;
            Instantiate(minion, new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z), Quaternion.identity);
        
    }
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            gold += goldPerSecond;
        }
        public void Save()
        {
        PlayerPrefs.SetFloat("gold", gold);
        PlayerPrefs.SetFloat("goldPerClick", goldPerClick);
        PlayerPrefs.SetFloat("goldPerSecond", goldPerSecond);
        PlayerPrefs.SetFloat("doransBladeCost", doransBladeCost);
        PlayerPrefs.SetFloat("doransRingCost", doransRingCost);
        PlayerPrefs.SetFloat("needlesslyLageRodCost", needlesslyLargeRodCost);
        PlayerPrefs.SetFloat("lichBaneCost", lichBaneCost);
        PlayerPrefs.SetFloat("mortalReminderCost", mortalReminderCost);
        PlayerPrefs.SetFloat("rageBladeCost", rageBladeCost);
        PlayerPrefs.SetFloat("infinityEdgeCost", infinityEdgeCost);
        PlayerPrefs.SetFloat("rabadonsDeathCapCost", rabadonsDeathCapCost);
        PlayerPrefs.SetFloat("TriForceCost", triForceCost);
    }
        public void Reset()
    {
        gold = 0;
        goldPerClick = 1;
        doransRingCost = 100;
        doransBladeCost = 100;
        recurveBowCost = 1250;
        needlesslyLargeRodCost = 1600;
        rageBladeCost = 3000;
        mortalReminderCost = 10590;
        lichBaneCost = 25500;
        goldPerSecond = 0;
        infinityEdgeCost = 450000000;
        rabadonsDeathCapCost = 450000000;
        triForceCost = 1900000000;
        PlayerPrefs.SetFloat("gold", gold);
        PlayerPrefs.SetFloat("goldPerClick", goldPerClick);
        PlayerPrefs.SetFloat("goldPerSecond", goldPerSecond);
        PlayerPrefs.SetFloat("doransBladeCost", doransBladeCost);
        PlayerPrefs.SetFloat("doransRingCost", doransRingCost);
        PlayerPrefs.SetFloat("needlesslyLageRodCost", needlesslyLargeRodCost);
        PlayerPrefs.SetFloat("lichBaneCost", lichBaneCost);
        PlayerPrefs.SetFloat("mortalReminderCost", mortalReminderCost);
        PlayerPrefs.SetFloat("rageBladeCost", rageBladeCost);
        PlayerPrefs.SetFloat("infinityEdgeCost", infinityEdgeCost);
        PlayerPrefs.SetFloat("rabadonsDeathCap", rabadonsDeathCapCost);
        PlayerPrefs.SetFloat("TriForceCost", triForceCost);
        if (levelUp >= 1)
        {
            lv1.SetActive(true);
            lv2.SetActive(false);
        }
    }
        public void DoransRing()
        {
            if(gold >= doransRingCost)
            {
                
                Souns.Instance.PlaySound(clickSound);
                gold -= doransRingCost;
                goldPerSecond += 1;
                Mathf.Round(doransRingCost *= costIncrease);
                doransRingCost = Mathf.Ceil(doransRingCost);
                Debug.Log(doransRingCost);
            }
        }
            public void DoransBlade()
            {
                if (gold >= doransBladeCost)
                {
                    
                    Souns.Instance.PlaySound(clickSound);
                    gold -= doransBladeCost;
                    goldPerClick += 1;
                    Mathf.Round(doransBladeCost *= costIncrease);
                    doransBladeCost = Mathf.Ceil(doransBladeCost);
                    Debug.Log(doransBladeCost);
                }
            }
            public void RecurveBow()
            {
                if(gold >= recurveBowCost)
                {
                    
                    Souns.Instance.PlaySound(clickSound);
                    gold -= recurveBowCost;
                    goldPerClick += 7;
                    recurveBowCost = Mathf.Ceil(recurveBowCost * costIncrease);
                }
            }
            public void NeedlesslyLargeRod()
     {
            if(gold >= needlesslyLargeRodCost)
            {
               
                Souns.Instance.PlaySound(clickSound);
                gold -= needlesslyLargeRodCost;
                goldPerSecond += 8;
                needlesslyLargeRodCost = Mathf.Ceil(needlesslyLargeRodCost * costIncrease);
            }
        }
            public void RageBlade()
    {
        if (gold >= rageBladeCost)
        {
        
            Souns.Instance.PlaySound(clickSound);
            gold -= rageBladeCost;
            goldPerSecond += 10;
            goldPerClick += 10;
            rageBladeCost = Mathf.Ceil(rageBladeCost * costIncrease);
        }
    }
            public void LichBane()
    {
        if (gold >= lichBaneCost)
        {
         
            Souns.Instance.PlaySound(clickSound);
            gold -= lichBaneCost;
            goldPerSecond += 5;
            goldPerClick *= 1.15f;
            lichBaneCost = Mathf.Ceil(lichBaneCost * costIncrease);
            goldPerClick = Mathf.Floor(goldPerClick);
        }
       }
            public void MortalReminder()
    {
        if (gold >= mortalReminderCost)
        {
          
            Souns.Instance.PlaySound(clickSound);
            gold -= mortalReminderCost;
            goldPerClick += 30;
            mortalReminderCost = Mathf.Ceil(mortalReminderCost * costIncrease);
        }
    }
        public void InfinityEdge()
    {
        if (gold >= infinityEdgeCost)
        {
            
            Souns.Instance.PlaySound(clickSound);
            gold -= infinityEdgeCost;
            goldPerClick += 10000;
            infinityEdgeCost = Mathf.Ceil(infinityEdgeCost * costIncrease);
        }
    }
        public void RabadonsDeathCap()
    {
        if (gold >= rabadonsDeathCapCost)
        {
           
            Souns.Instance.PlaySound(clickSound);
            gold -= rabadonsDeathCapCost;
            goldPerSecond += 1000;
            rabadonsDeathCapCost = Mathf.Ceil(rabadonsDeathCapCost * costIncrease);
        }
    }
    public void TriForce()
    {
        if (gold >= triForceCost)
        {
            Souns.Instance.PlaySound(clickSound);
            gold -= triForceCost;
            goldPerSecond += 1000;
            goldPerClick += 10000;
            goldPerClick *= 1.1f;
            goldPerSecond *= 1.1f;
            rabadonsDeathCapCost = Mathf.Ceil(rabadonsDeathCapCost * costIncrease);
            goldPerClick = Mathf.Floor(goldPerClick);
            goldPerSecond = Mathf.Floor(goldPerSecond);
            //final item used to get infinity
        }
    }
}
